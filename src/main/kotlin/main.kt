
class romanNumerals(){

    val roman = sortedMapOf(Comparator.reverseOrder<Int>(),Pair(1000,"M"),Pair(900,"CM"),Pair(500,"D"),Pair(400,"CD"),
        Pair(100,"C"),Pair(90,"XC"),Pair(50,"L"),Pair(40,"XL"),Pair(10,"X"),Pair(9,"IX"),Pair(5,"V"),Pair(4,"IV"),
        Pair(1,"I"))

    fun convert(i: Int): String {
        var romanNumber :String=""
        var arabicPointer= i

        for(actualKey in roman.keys) {
            while (arabicPointer >= actualKey) {
                arabicPointer=arabicPointer-actualKey
                romanNumber=romanNumber+roman[actualKey]
            }
        }
        print(romanNumber)
        return romanNumber
    }
}