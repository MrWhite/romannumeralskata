import org.junit.jupiter.api.Test

class romanNumeralsTest {

    @Test
    fun romanConversionOneIsI() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(1).equals("I"))
    }

    @Test
    fun romanConversionFiveIsV() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(5).equals("V"))
    }

    @Test
    fun romanConversionThreeIsIII() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(3).equals("III"))
    }
    @Test
    fun romanConversion1978IsMCMLXXVIII() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(1978).equals("MCMLXXVIII"))
    }
    @Test
    fun romanConversion677IsDCLXXVII() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(677).equals("DCLXXVII"))
    }
    @Test
    fun romanConversion998IsCMXCVIII() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(998).equals("CMXCVIII"))
    }
    @Test
    fun romanConversion84IsLXXXIV() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(84).equals("LXXXIV"))
    }
    @Test
    fun romanConversion40IsXL() {

        val romanNumerals = romanNumerals()
        assert(romanNumerals.convert(40).equals("XL"))
    }
}